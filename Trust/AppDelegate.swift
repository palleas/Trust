//
//  AppDelegate.swift
//  Trust
//
//  Created by Romain Pouclet on 2015-11-20.
//  Copyright © 2015 Perfectly-Cooked. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: Colors.textColor]
        UINavigationBar.appearance().barTintColor = Colors.background
        UINavigationBar.appearance().tintColor = Colors.textColor
        UINavigationBar.appearance().backgroundColor = Colors.background
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarPosition: .Any, barMetrics: .Default)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: Colors.textColor], forState: .Normal)
        
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.rootViewController = RootViewController()
        window?.backgroundColor = Colors.background
        window?.makeKeyAndVisible()
        
        return true
    }

}

