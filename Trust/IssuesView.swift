//
//  IssuesView.swift
//  Trust
//
//  Created by Romain Pouclet on 2015-11-21.
//  Copyright © 2015 Perfectly-Cooked. All rights reserved.
//

import UIKit

class IssuesView: UIView {

    @IBOutlet weak var issuesCountLabel: UILabel! {
        didSet {
            issuesCountLabel.textColor = Colors.textColor
        }
    }

    @IBOutlet weak var issuesTableView: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = Colors.background
    }
    
    var issuesCount: Int = 0 {
        didSet {
            issuesCountLabel.text = String(issuesCount)
        }
    }
}
