//
//  IssuesViewController.swift
//  Trust
//
//  Created by Romain Pouclet on 2015-11-21.
//  Copyright © 2015 Perfectly-Cooked. All rights reserved.
//

import UIKit
import ReactiveCocoa

class IssuesViewController: UIViewController {
    let client = OctoClient()
    
    var issuesView: IssuesView {
        get {
            return view as! IssuesView
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        title = "Issues"
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "burger-menu"), landscapeImagePhone: nil, style: .Plain, target: self, action: Selector("didTapMenuButton"))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "octopus-menu"), landscapeImagePhone: nil, style: .Plain, target: self, action: Selector("didTapUserButton"))
        
        var count: Int = 0
        
        client
            .findPopularRepositories()
            .observeOn(UIScheduler())
            .on(started: { count = 0 }, next: { _ in
                count++
                UIScheduler().schedule({ () -> () in
                    self.issuesView.issuesCount = count
                })
            })
            .collect()
            .startWithNext { (repositories) -> () in
                print(repositories.count)
            }
    }

    func didTapMenuButton() {
        
    }
    
    func didTapUserButton() {
        
    }
}
