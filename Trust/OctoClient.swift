//
//  OctoClient.swift
//  Trust
//
//  Created by Romain Pouclet on 2015-11-21.
//  Copyright © 2015 Perfectly-Cooked. All rights reserved.
//

import UIKit
import ReactiveCocoa

class OctoClient: NSObject {
    
    struct Repository {
        let id: Int
        let name: String
    }
    
    func findPopularRepositories() -> SignalProducer<Repository, NSError> {
        let request = NSURLRequest(URL: NSURL(string: "https://api.github.com/search/repositories?q=swift+language:swift&sort=stars&order=desc")!)
        
        return NSURLSession.sharedSession()
            .rac_dataWithRequest(request)
            .map({ (data, _) -> [[String: AnyObject]] in
                if let mapped = try? NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) {
                    return mapped["items"] as! [[String: AnyObject]]
                }
                
                return [[String: AnyObject]]()
            })
            .flatMap(FlattenStrategy.Latest, transform: { (items) -> SignalProducer<Repository, NSError> in
                let producer = SignalProducer<[String: AnyObject], NSError>(values: items)
                return producer.map({ (item) -> Repository in
                    return Repository(id: item["id"] as! Int , name: item["name"] as! String)
                })
            })
    }
}
