//
//  OnboardingView.swift
//  Trust
//
//  Created by Romain Pouclet on 2015-11-21.
//  Copyright © 2015 Perfectly-Cooked. All rights reserved.
//

import UIKit

protocol OnboardingViewDelegate: class {
    func didSelectLoginAction()
    func didSelectViewPopularIssuesAction()
}

class OnboardingView: UIView {
    
    weak var delegate: OnboardingViewDelegate?

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Colors.textColor
        }
    }
    
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.setTitleColor(Colors.textColor, forState: .Normal)
            loginButton.backgroundColor = Colors.login
        }
    }
    
    @IBOutlet weak var orLabel: UILabel! {
        didSet {
            orLabel.textColor = Colors.lightTextColor
        }
    }
    
    @IBOutlet weak var popularIssuesButton: UIButton! {
        didSet {
            popularIssuesButton.setTitleColor(Colors.textColor, forState: .Normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = Colors.modalBackground
        loginButton.addTarget(self, action: Selector("didSelectLogin"), forControlEvents: .TouchUpInside)
        popularIssuesButton.addTarget(self, action: Selector("didSelectPopularIssues"), forControlEvents: .TouchUpInside)
    }
    
    func didSelectLogin() {
        delegate?.didSelectLoginAction()
    }
    
    func didSelectPopularIssues() {
        delegate?.didSelectViewPopularIssuesAction()
    }
}
