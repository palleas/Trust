//
//  OnboardingViewController.swift
//  Trust
//
//  Created by Romain Pouclet on 2015-11-21.
//  Copyright © 2015 Perfectly-Cooked. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {
    enum Action {
        case Login
        case ViewPopularIssues
    }
    
    typealias Completion = (action: Action) -> Void
    
    let completion: Completion
    
    var mainView: OnboardingView {
        get {
            return view as! OnboardingView
        }
    }
    
    init(completion: Completion) {
        self.completion = completion
        
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
    }
}

extension OnboardingViewController: OnboardingViewDelegate {
    func didSelectLoginAction() {
        completion(action: .Login)
    }
    
    func didSelectViewPopularIssuesAction() {
        completion(action: .ViewPopularIssues)
    }
}