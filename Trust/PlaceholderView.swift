//
//  PlaceholderView.swift
//  Trust
//
//  Created by Romain Pouclet on 2015-11-21.
//  Copyright © 2015 Perfectly-Cooked. All rights reserved.
//

import UIKit

class PlaceholderView: UIView {
    let octopus = UIImageView(image: UIImage(named: "octopus-icon"))

    init() {
        super.init(frame: CGRectZero)
        
        addSubview(octopus)
        
        octopus.translatesAutoresizingMaskIntoConstraints = false
        octopus.centerXAnchor.constraintEqualToAnchor(centerXAnchor).active = true
        octopus.centerYAnchor.constraintEqualToAnchor(centerYAnchor).active = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
