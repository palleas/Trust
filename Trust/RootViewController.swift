//
//  RootViewController.swift
//  Trust
//
//  Created by Romain Pouclet on 2015-11-21.
//  Copyright © 2015 Perfectly-Cooked. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    var onboarded = false // I'm sorry
    
    var rootView: RootView {
        get {
            return view as! RootView
        }
    }
    
    override func loadView() {
        view = RootView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
//        if !onboarded {
//            let onboarding = OnboardingViewController { (action) -> Void in
//                self.dismissViewControllerAnimated(true, completion: nil)
//                self.onboarded = true
//                
//                let navigation = UINavigationController(rootViewController: IssuesViewController())
//                self.transitionToViewController(navigation)
//            }
//            onboarding.modalPresentationStyle = .OverFullScreen
//            presentViewController(onboarding, animated: true, completion: nil)
//        }
        
        let navigation = UINavigationController(rootViewController: IssuesViewController())
        self.transitionToViewController(navigation)

    }
    
    func transitionToViewController(controller: UIViewController) {
        addChildViewController(controller)
        controller.willMoveToParentViewController(self)
        rootView.transitionToView(controller.view)
        controller.didMoveToParentViewController(self)
    }
}
