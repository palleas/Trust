//
//  Stylesheet.swift
//  Trust
//
//  Created by Romain Pouclet on 2015-11-20.
//  Copyright © 2015 Perfectly-Cooked. All rights reserved.
//

import Foundation
import HEXColor

struct Colors {
    static let firstLogoPart = UIColor(hex6: 0xB98FDE)
    static let secondLogoPart = UIColor(hex6: 0x48C99E)
    static let thirdLogoPart = UIColor(hex6: 0xFF465D)
    static let fourthLogoPart = UIColor(hex6: 0xFFF790)
    static let background = UIColor(hex6: 0x222222)
    static let modalBackground = UIColor(hex6: 0x232323).colorWithAlphaComponent(0.90)
    static let issueNumber = UIColor.whiteColor().colorWithAlphaComponent(0.38)
    static let issueTitle = UIColor.whiteColor()
    static let navigationSeparator = UIColor(hex6: 0x979797)
    static let logout = UIColor(hex6: 0xFF465D)
    static let login = UIColor(hex6: 0x222222)

    static let textColor = UIColor.whiteColor()
    static let lightTextColor = UIColor(hex6: 0x545454)
}